import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'

//  User
import NewUser from './views/user/NewUser.vue'
import UserList from './views/user/UserList.vue'
import EditUser from './views/user/EditUser.vue'
// Supplier
import NewSupplier from './views/supplier/NewSupplier.vue'
import SupplierList from './views/supplier/SupplierList.vue'
import EditSupplier from './views/supplier/EditSupplier.vue'
// Customer
import NewCustomer from './views/customer/NewCustomer.vue'
import CustomerList from './views/customer/CustomerList.vue'
import EditCustomer from './views/customer/EditCustomer.vue'
// Configuration
import ColorList from './views/configuration/ColorList.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/home',
      name: 'home',
      component: Home
    },
    {
      path: '/user/new-user',
      name: 'new-user',
      component: NewUser
    },
    {
      path: '/user/edit-user/:idUser',
      name: 'edit-user',
      component: EditUser
    },
    {
      path: '/user/user-list',
      name: 'user-list',
      component: UserList
    },
    {
      path: '/supplier/new-supplier',
      name: 'new-supplier',
      component: NewSupplier
    },
    {
      path: '/supplier/edit-supplier/:idSupplier',
      name: 'edit-supplier',
      component: EditSupplier
    },
    {
      path: '/supplier/supplier-list',
      name: 'supplier-list',
      component: SupplierList
    },
    {
      path: '/customer/new-customer',
      name: 'new-customer',
      component: NewCustomer
    },
    {
      path: '/customer/customer-list',
      name: 'customer-list',
      component: CustomerList
    },
    {
      path: '/customer/edit-customer/:idCustomer',
      name: 'edit-customer',
      component: EditCustomer
    },
    {
      path: '/configuration/color-list',
      name: 'color-list',
      component: ColorList
    }
  ]
})
