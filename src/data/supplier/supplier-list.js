export default [
  {
    key: 'name',
    label: 'Nombre Proveedor',
    sortable: true
  },
  {
    key: 'address',
    label: 'Dirección'
  },
  {
    key: 'phoneNumber',
    label: 'Teléfono'
  },
  {
    key: 'ownerName',
    label: 'Encargado',
    sortable: true
  },
  {
    key: 'id',
    label: 'Acciones'
  }
]
