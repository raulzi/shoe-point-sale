export default [
  {
    header: true,
    title: 'Zapatería Monse'
  },
  {
    title: 'Venta',
    icon: 'fas fa-cash-register',
    href: '/home'
  },
  {
    title: 'Clientes',
    icon: 'fas fa-id-card',
    child: [
      {
        title: 'Agregar Cliente',
        icon: 'fas fa-plus-square',
        href: '/customer/new-customer'
      },
      {
        title: 'Lista de Clientes',
        icon: 'fas fa-list',
        href: '/customer/customer-list'
      }
    ]
  },
  {
    title: 'Corte de Caja',
    icon: 'fas fa-money-bill-alt'
  },
  {
    title: 'Notas',
    icon: 'fas fa-file-invoice-dollar'
  },
  {
    title: 'Usuario',
    icon: 'fa fa-user',
    child: [
      {
        title: 'Agrega Usuario',
        icon: 'fas fa-plus-square',
        href: '/user/new-user'
      },
      {
        title: 'Lista de Usuarios',
        icon: 'fas fa-list',
        href: '/user/user-list'
      }
    ]
  },
  {
    title: 'Inventario',
    icon: 'fa fa-box'
  },
  {
    title: 'Proovedores',
    icon: 'fa fa-parachute-box',
    child: [
      {
        title: 'Agrega Proveedor',
        icon: 'fas fa-plus-square',
        href: '/supplier/new-supplier'
      },
      {
        title: 'Lista de Proovedores',
        icon: 'fas fa-list',
        href: '/supplier/supplier-list'
      }
    ]
  },
  {
    title: 'Configuración',
    icon: 'fa fa-cogs',
    child: [
      {
        title: 'Colores',
        icon: 'fas fa-palette',
        href: '/configuration/color-list'
      },
      {
        title: 'Numeración',
        icon: 'fas fa-ruler-horizontal'
      },
      {
        title: 'Rol',
        icon: 'fa fa-user-tag'
      }
    ]
  },
  {
    title: 'Cerrar Sesión',
    icon: 'fa fa-sign-out-alt',
    attributes: {
      item: 'logout'
    }
  }
]
