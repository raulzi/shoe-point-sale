export default [
  {
    key: 'username',
    label: 'Usuario',
    sortable: true
  },
  {
    key: 'name',
    label: 'Nombre',
    sortable: true
  },
  {
    key: 'lastName',
    label: 'Apellido',
    sortable: true
  },
  {
    key: 'role',
    label: 'Rol'
  },
  {
    key: 'active',
    label: 'Activo'
  },
  {
    key: 'id',
    label: 'Acciones'
  }
]
