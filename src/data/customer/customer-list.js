export default [
  {
    key: 'name',
    label: 'Nombre Cliente',
    sortable: true
  },
  {
    key: 'address',
    label: 'Dirección'
  },
  {
    key: 'phoneNumber',
    label: 'Teléfono'
  },
  {
    key: 'id',
    label: 'Acciones'
  }
]
