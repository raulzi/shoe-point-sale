import menu from '@/data/menu'
import userListFields from '@/data/user/user-list'
import supplierListFields from '@/data/supplier/supplier-list'
import customerListFields from '@/data/customer/customer-list'
import colorListFields from '@/data/configuration/color-list'

export default {
  menu,
  userListFields,
  supplierListFields,
  customerListFields,
  colorListFields
}
